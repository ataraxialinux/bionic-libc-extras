#include "internal.h"

#include <stdio.h>

int main()
{
	char buf[4096];
	char buf2[4096];
	struct passwd pwd;
	struct passwd *pwdx;
	struct spwd spwd;
	struct spwd *spwdx;
	FILE *fp = fopen("/etc/passwd", "r");
	FILE *fp2 = fopen("/home/yuyu/junk/shadow_lol", "r");

	parsePasswd(fp, &pwd, buf, sizeof(buf), &pwdx);
	parseShadow(fp2, &spwd, buf2, sizeof(buf2), &spwdx);

	printf("passwd: %s %s\n", pwdx->pw_name, pwdx->pw_shell);
	printf("shadow: %s %ld\n", spwdx->sp_namp, spwdx->sp_flag);

	return 0;
}
