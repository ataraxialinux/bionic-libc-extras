#ifndef _PWD_H
#define _PWD_H

#include <sys/types.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

struct passwd {
	char *pw_name;
	char *pw_passwd;
	uid_t pw_uid;
	gid_t pw_gid;
	char *pw_gecos;
	char *pw_dir;
	char *pw_shell;
};

void setpwent(void);
void endpwent(void);

struct passwd *getpwent(void);
struct passwd *getpwuid(uid_t uid);
struct passwd *getpwnam(const char *name);
struct passwd *fgetpwent(FILE *stream);

int putpwent(const struct passwd *pwd, FILE *fp);

int getpwent_r(struct passwd *resbuf, char *buf, size_t len, struct passwd **res);
int getpwuid_r(uid_t uid, struct passwd *resbuf, char *buf, size_t len, struct passwd **res);
int getpwnam_r(const char *name, struct passwd *resbuf, char *buf, size_t len, struct passwd **res);
int fgetpwent_r(FILE *stream, struct passwd *resbuf, char *buf, size_t len, struct passwd **res);

int getpw(uid_t uid, char *buf);

#ifdef __cplusplus
}
#endif

#endif
