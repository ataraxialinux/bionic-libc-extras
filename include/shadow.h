#ifndef _SHADOW_H
#define _SHADOW_H

#include <sys/types.h>
#include <stdio.h>

#define	SHADOW "/etc/shadow"

#ifdef __cplusplus
extern "C" {
#endif

struct spwd {
	char *sp_namp;
	char *sp_pwdp;
	long sp_lstchg;
	long sp_min;
	long sp_max;
	long sp_warn;
	long sp_inact;
	long sp_expire;
	unsigned long sp_flag;
};

void setspent(void);
void endspent(void);

struct spwd *getspent(void);
struct spwd *getspnam(const char *name);
struct spwd *fgetspent(FILE *stream);
struct spwd *sgetspent(const char *str);

int putspent(const struct spwd *sp, FILE *fp);

int getspent_r(struct spwd *resbuf, char *buf, size_t len, struct spwd **res);
int getspnam_r(const char *name, struct spwd *resbuf, char *buf, size_t len, struct spwd **res);
int fgetspent_r(FILE *stream, struct spwd *resbuf, char *buf, size_t len, struct spwd **res);
int sgetspent_r(const char *str, struct spwd *resbuf, char *buf, size_t len, struct spwd **res);

int lckpwdf(void);
int ulckpwdf(void);

#ifdef __cplusplus
}
#endif

#endif
