#include "shadow.h"
#include "internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define __UCLIBC_PWD_BUFFER_SIZE__ 256
#define NUM(n) ((n) == -1 ? 0 : -1), ((n) == -1 ? 0 : (n))
#define STR(s) ((s) ? (s) : "")

static FILE *f;

void setspent(void)
{
	if (f)
		fclose(f);

	f = 0;
}

void endspent(void)
{
	setspent();
}

int lckpwdf(void)
{
	return 0;
}

int ulckpwdf(void)
{
	return 0;
}

int fgetspent_r(FILE *stream, struct spwd *resbuf, char *buf, size_t len, struct spwd **res)
{
	int p;
	*res = NULL;

	if (!stream) {
		errno = ENOENT;
		return -1;
	}

	p = __pgsreader(__parsespent, resbuf, buf, len, stream);
	if (p != 0)
		return -1;

	*res = resbuf;

	return 0;
}

int sgetspent_r(const char *str, struct spwd *resbuf, char *buf, size_t len, struct spwd **res)
{
	*res = NULL;

	if (len < __UCLIBC_PWD_BUFFER_SIZE__) {
		errno = ERANGE;
		return -1;
	}

	if (str != buf) {
		if (strlen(str) >= len) {
			errno = ERANGE;
			return -1;
		}
			
		strcpy(buf, str);
	}

	if (!(__parsespent(resbuf, buf)))
		*res = resbuf;

	return 0;
}

int getspent_r(struct spwd *resbuf, char *buf, size_t len, struct spwd **res)
{
	int p;
	*res = NULL;

	if (!f)
		f = fopen("/etc/shadow", "r");

	if (!f) {
		errno = ENOENT;
		return -1;
	}

	p = __pgsreader(__parsespent, resbuf, buf, len, f);
	if (p != 0)
		return -1;

	*res = resbuf;

	return 0;
}

int getspnam_r(const char *name, struct spwd *resbuf, char *buf, size_t len, struct spwd **res)
{
	int p;
	*res = NULL;

	if (!f)
		f = fopen("/etc/shadow", "r");

	if (!f) {
		errno = ENOENT;
		return -1;
	}

	do {
		p = __pgsreader(__parsespent, resbuf, buf, len, f);
		if (p != 0)
			return -1;

		if (!strcmp(resbuf->sp_namp, name)) {
			*res = resbuf;
			break;
		}
	} while (1);

	return 0;
}

struct spwd *fgetspent(FILE *stream)
{
	int p;
	char *line = NULL;
	struct spwd buf;
	struct spwd *result;

	if (!line) line = malloc(__UCLIBC_PWD_BUFFER_SIZE__);
	if (!line) return 0;

	p = fgetspent_r(stream, &buf, line, __UCLIBC_PWD_BUFFER_SIZE__, &result);
	if (p) {
		result = NULL;
		errno = ENOENT;
	}

	return result;
}

struct spwd *sgetspent(const char *str)
{
	int p;
	char *line = NULL;
	struct spwd buf;
	struct spwd *result;

	if (!line) line = malloc(__UCLIBC_PWD_BUFFER_SIZE__);
	if (!line) return 0;

	p = sgetspent_r(str, &buf, line, __UCLIBC_PWD_BUFFER_SIZE__, &result);
	if (p) {
		result = NULL;
		errno = p;
	}

	return result;
}

struct spwd *getspent(void)
{
	int p;
	char *line = NULL;
	struct spwd buf;
	struct spwd *result;

	if (!line) line = malloc(__UCLIBC_PWD_BUFFER_SIZE__);
	if (!line) return 0;

	p = getspent_r(&buf, line, __UCLIBC_PWD_BUFFER_SIZE__, &result);
	if (p) {
		result = NULL;
		errno = p;
	}

	return result;
}

struct spwd *getspnam(const char *name)
{
	int p;
	char *line = NULL;
	struct spwd buf;
	struct spwd *result;

	if (!line) line = malloc(__UCLIBC_PWD_BUFFER_SIZE__);
	if (!line) return 0;

	p = getspnam_r(name, &buf, line, __UCLIBC_PWD_BUFFER_SIZE__, &result);
	if (p) {
		result = NULL;
		errno = p;
	}

	return result;
}

int putspent(const struct spwd *sp, FILE *fp)
{
	int p;

	p = fprintf(fp, "%s:%s:%.*ld:%.*ld:%.*ld:%.*ld:%.*ld:%.*ld:%.*lu\n",
		STR(sp->sp_namp), STR(sp->sp_pwdp), NUM(sp->sp_lstchg),
		NUM(sp->sp_min), NUM(sp->sp_max), NUM(sp->sp_warn),
		NUM(sp->sp_inact), NUM(sp->sp_expire), NUM(sp->sp_flag));

	if (p < 0)
		return -1;

	return 0;
}
