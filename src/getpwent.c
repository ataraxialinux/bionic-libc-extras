#define _GNU_SOURCE

#include "pwd.h"
#include "internal.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

static FILE *f;
static char *line;
static size_t size = 0;
static struct passwd buf;

void setpwent(void)
{
	if (f)
		fclose(f);

	f = 0;
}

void endpwent(void)
{
	setpwent();
}

int fgetpwent_r(FILE *stream, struct passwd *resbuf, char *buf, size_t len, struct passwd **res)
{
	int p;
	*res = NULL;

	if (!stream) {
		errno = ENOENT;
		return -1;
	}

	p = __pgsreader(__parsepwent, resbuf, buf, len, stream);
	if (p != 0)
		return -1;

	*res = resbuf;

	return 0;
}

int getpwent_r(struct passwd *resbuf, char *buf, size_t len, struct passwd **res)
{
	int p;
	*res = NULL;

	if (!f)
		f = fopen("/etc/passwd", "r");

	if (!f) {
		errno = ENOENT;
		return -1;
	}

	p = __pgsreader(__parsepwent, resbuf, buf, len, f);
	if (p != 0)
		return -1;

	*res = resbuf;

	return 0;
}

int getpwuid_r(uid_t uid, struct passwd *resbuf, char *buf, size_t len, struct passwd **res)
{
	int p;
	*res = NULL;

	if (!f)
		f = fopen("/etc/passwd", "r");

	if (!f) {
		errno = ENOENT;
		return -1;
	}

	do {
		p = __pgsreader(__parsepwent, resbuf, buf, len, f);
		if (p != 0)
			return -1;

		if (resbuf->pw_uid == uid) {
			*res = resbuf;
			break;
		}
	} while (1);

	return 0;
}

int getpwnam_r(const char *name, struct passwd *resbuf, char *buf, size_t len, struct passwd **res)
{
	int p;
	*res = NULL;

	if (!f)
		f = fopen("/etc/passwd", "r");

	if (!f) {
		errno = ENOENT;
		return -1;
	}

	do {
		p = __pgsreader(__parsepwent, resbuf, buf, len, f);
		if (p != 0)
			return -1;

		if (strcmp(resbuf->pw_name, name)) {
			*res = resbuf;
			break;
		}
	} while (1);

	return 0;
}

struct passwd *fgetpwent(FILE *stream)
{
	int p;
	struct passwd *result;
	p = fgetpwent_r(stream, &buf, line, size, &result);
	if (p != 0) {
		result = NULL;
		errno = ENOENT;
	}
	return result;
}

struct passwd *getpwent(void)
{
	int p;
	struct passwd *result;
	p = getpwent_r(&buf, line, size, &result);
	if (p != 0) {
		result = NULL;
		errno = p;
	}
	return result;
}

struct passwd *getpwuid(uid_t uid)
{
	int p;
	struct passwd *result;
	p = getpwuid_r(uid, &buf, line, size, &result);
	if (p != 0) {
		result = NULL;
		errno = p;
	}
	return result;
}

struct passwd *getpwnam(const char *name)
{
	int p;
	struct passwd *result;
	p = getpwnam_r(name, &buf, line, size, &result);
	if (p != 0) {
		result = NULL;
		errno = p;
	}
	return result;
}

int putpwent(const struct passwd *pwd, FILE *fp)
{
	int p;

	p = fprintf(fp, "%s:%s:%u:%u:%s:%s:%s\n",
		pwd->pw_name, pwd->pw_passwd,
		pwd->pw_uid, pwd->pw_gid, pwd->pw_gecos,
		pwd->pw_dir, pwd->pw_shell);
	if (p < 0)
		return -1;

	return 0;
}

int getpw(uid_t uid, char *buf)
{
	int p;
	struct passwd pf;
	struct passwd *pwd;

	if (!buf) {
		errno = EINVAL;
		return -1;
	}

	p = getpwuid_r(uid, &pf, line, size, &pwd);
	if (p < 0)
		return -1;

	p = asprintf(&buf, "%s:%s:%u:%u:%s:%s:%s\n",
		pwd->pw_name, pwd->pw_passwd,
		pwd->pw_uid, pwd->pw_gid, pwd->pw_gecos,
		pwd->pw_dir, pwd->pw_shell);
	if (p < 0)
		return -1;

	return 0;
}
