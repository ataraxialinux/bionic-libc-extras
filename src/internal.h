#ifndef _INTERNAL_H
#define _INTERNAL_H

#include "grp.h"
#include "pwd.h"
#include "shadow.h"

#include <pthread.h>

#define _MUTEX_LOCK(l) pthread_mutex_lock((pthread_mutex_t*) l)
#define _MUTEX_UNLOCK(l) pthread_mutex_unlock((pthread_mutex_t*) l)

__attribute__((visibility("hidden"))) int __parsepwent(void *pw, char *line);
__attribute__((visibility("hidden"))) int __parsegrent(void *gr, char *line);
__attribute__((visibility("hidden"))) int __parsespent(void *sp, char *line);
__attribute__((visibility("hidden"))) int __pgsreader(int (*__parserfunc)(void *d, char *line), void *data,
		char *__restrict line_buff, size_t buflen, FILE *f);

#endif
